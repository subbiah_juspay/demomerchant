package in.juspay.demomerchant;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Date;


/**
 * Created by MVMS1994 on 23/06/16.
 */
public class MainActivity extends AppCompatActivity {
    static String bank;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setDefault();

        url = getResources().getString(R.string.host);
        Button btn1 = (Button) findViewById(R.id.cont1);
        Button btn2 = (Button) findViewById(R.id.cont2);

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeOrder();
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeOrder();
            }
        });
    }

    private JSONObject prepOrder() {
        try {
            JSONObject value = new JSONObject();
            JSONObject data = new JSONObject();
            data.put("amount", "Rs. 5999");
            data.put("product", "KINDLE");
            data.put("merchant", "AMAZON");
            value.put("data", data);
            value.put("status", "PENDING");
            value.put("order_id", "#"+(new Date()).getTime());
            return value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void placeOrder() {
        final RequestQueue queue = Volley.newRequestQueue(this);
        final JSONObject responseData = prepOrder();
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url+"/store", responseData,
                new Response.Listener<JSONObject>() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onResponse(JSONObject response) {
                        Intent in = new Intent(MainActivity.this, WebActivity.class);
                        in.putExtra("bank", MainActivity.bank);
                        startActivity(in);
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Response-Error", error.getMessage(), error);
                    }
                });
        queue.add(jsonObjectRequest);
    }

    private void setDefault() {
        final RadioButton
            netbtn = (RadioButton) findViewById(R.id.nb),
            ccbtn = (RadioButton) findViewById(R.id.cc),
            dcbtn = (RadioButton) findViewById(R.id.dc),
            codbtn = (RadioButton) findViewById(R.id.cod);
        final Spinner spinner = (Spinner) findViewById(R.id.bank_menu);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bank = spinner.getSelectedItem().toString();
                bank = bank.toUpperCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        netbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccbtn.setChecked(false);
                dcbtn.setChecked(false);
                codbtn.setChecked(false);
                spinner.setVisibility(View.VISIBLE);
            }
        });
        ccbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                netbtn.setChecked(false);
                dcbtn.setChecked(false);
                codbtn.setChecked(false);
                spinner.setVisibility(View.GONE);
            }
        });
        dcbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccbtn.setChecked(false);
                netbtn.setChecked(false);
                codbtn.setChecked(false);
                spinner.setVisibility(View.GONE);
            }
        });
        codbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccbtn.setChecked(false);
                dcbtn.setChecked(false);
                netbtn.setChecked(false);
                spinner.setVisibility(View.GONE);
            }
        });
        netbtn.setChecked(true);
    }
}
