package in.juspay.demomerchant;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by MVMS1994 on 23/06/16.
 */
public class WebActivity extends AppCompatActivity{
    Button x;
    Dialog dialog;
    WebView webView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_activity);

        webView = (WebView) findViewById(R.id.activity_main_webview);
        final String bank = getIntent().getStringExtra("bank");
        assert webView != null;
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        }
        webView.loadUrl(getResources().getString(R.string.url) + "/loading_"+bank);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if(url.equals(getResources().getString(R.string.host) + "/HDFC")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.hdfc");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                } else if(url.equals(getResources().getString(R.string.host) + "/KOTAK")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.totp");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                } else if(url.equals(getResources().getString(R.string.host) + "/AXIS")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.axis");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.equals(getResources().getString(R.string.url) + "/HDFC")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.hdfc");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                    return false;
                } else if(url.equals(getResources().getString(R.string.url) + "/KOTAK")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.totp");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                } else if(url.equals(getResources().getString(R.string.url) + "/AXIS")) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("juspay.in.axis");
                    if(launchIntent != null) {
                        Toast.makeText(WebActivity.this, "Detected " + bank + " APP, Opening...", Toast.LENGTH_SHORT).show();
                        startActivity(launchIntent);
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();
        webView.loadUrl(getResources().getString(R.string.url) + "/return");
    }
}
